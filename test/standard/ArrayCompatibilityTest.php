<?php

use \phlint\Test as PhlintTest;

class ArrayCompatibilityTest {

  /**
   * Test implicit conversion to float[] with argument declaration.
   *
   * @test @internal
   */
  static function unittest_floatArrayTest () {
    PhlintTest::assertIssues('
      /**
       * @param float[] $bar
       */
      function foo ($bar) {}
      foo([5]);
      foo([5, 1.1]);
      foo([1.1]);
      foo([5, 1.1, "a"]);
      foo(["a"]);
      foo(1);
      foo("a");
      foo(false);
    ', [
      '
        Provided argument *[5, 1.1, "a"]* of type *(int|float|string)[]*
          is not implicitly convertable to type *float[]*
          in the expression *foo([5, 1.1, "a"])* on line 8.
      ',
      '
        Provided argument *["a"]* of type *string[]*
          is not implicitly convertable to type *float[]*
          in the expression *foo(["a"])* on line 9.
      ',
      '
        Provided argument *1* of type *int*
          is not implicitly convertable to type *float[]*
          in the expression *foo(1)* on line 10.
      ',
      '
        Provided argument *"a"* of type *string*
          is not implicitly convertable to type *float[]*
          in the expression *foo("a")* on line 11.
      ',
      '
        Provided argument *false* of type *bool*
          is not implicitly convertable to type *float[]*
          in the expression *foo(false)* on line 12.
      ',
    ]);
  }

  /**
   * Test implicit conversion to string[][string][] with argument declaration.
   *
   * @test @internal
   */
  static function unittest_mixedDimensionalStringArrayTest () {
    PhlintTest::assertIssues('
      /**
       * @param string[][string][] $bar
       */
      function foo ($bar) {}
      foo(false);
      foo(["a"]);
      foo([["a"]]);
      foo(["a" => ["a"]]);
      foo([["a" => ["a"]]]);
      foo([[1 => ["a"]]]);
    ', [
      '
        Provided argument *false* of type *bool*
          is not implicitly convertable to type *string[][string][]*
          in the expression *foo(false)* on line 5.
      ',
      '
        Provided argument *["a"]* of type *string[]*
          is not implicitly convertable to type *string[][string][]*
          in the expression *foo(["a"])* on line 6.
      ',
      '
        Provided argument *[["a"]]* of type *string[][]*
          is not implicitly convertable to type *string[][string][]*
          in the expression *foo([["a"]])* on line 7.
      ',
      '
        Provided argument *["a" => ["a"]]* of type *string[][string]*
          is not implicitly convertable to type *string[][string][]*
          in the expression *foo(["a" => ["a"]])* on line 8.
      ',
      '
        Provided argument *[[1 => ["a"]]]* of type *string[][][]*
          is not implicitly convertable to type *string[][string][]*
          in the expression *foo([[1 => ["a"]]])* on line 10.
      ',
    ]);
  }

  /**
   * Test implicit conversion to and from (int[]|string[])[] with argument declaration.
   *
   * @test @internal
   */
  static function unittest_mixedDimensionalIntAndStringArrayTest () {
    PhlintTest::assertIssues('

      function foo (int $baz) {}
      foo([[0], [""]]);
      foo(0);

      /**
       * @param (int[]|string[])[]
       */
      function bar ($baz) {}
      bar([[0], [""]]);
      bar(0);

    ', [
      '
        Provided argument *[[0], [""]]* of type *(int[]|string[])[]*
          is not implicitly convertable to type *int*
          in the expression *foo([[0], [""]])* on line 3.
      ',
      // @todo: 0 is not compatible with (int[]|string[])[]
    ]);
  }

}
