<?php

use \phlint\Test as PhlintTest;

class VariadicParameterTest {

  /**
   * Test variadic arguments against an array constraint.
   *
   * @test @internal
   */
  static function unittest_arrayConstraintTest () {
    PhlintTest::assertIssues('
      function foo(array ...$bar) {}
      foo([]);
      foo([""], [0]);
      foo(0);
    ', [
      '
        Provided argument *0* of type *int*
          is not implicitly convertable to type *array*
          in the expression *foo(0)* on line 4.
      ',
    ]);
  }

}
