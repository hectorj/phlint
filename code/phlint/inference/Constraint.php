<?php

namespace phlint\inference;

use \luka8088\phops as op;
use \phlint\IIData;
use \phlint\inference;
use \phlint\NodeConcept;
use \phlint\phpLanguage;
use \PhpParser\Comment;
use \PhpParser\Node;

/**
 * @see /documentation/constraint/index.md
 */
class Constraint {

  function getIdentifier () {
    return 'constraint';
  }

  function getPass () {
    return 30;
  }

  function visitNode ($node) {

    if (!$node->getAttribute('inAnalysisScope', true))
      return;

    inference\Constraint::inferConstraints($node);

  }

  static function inferConstraints ($node) {

    if (!NodeConcept::isExecutionContextNode($node))
      return;

    if (isset(op\metaContext(IIData::class)['areConstraintsInferred:' . spl_object_hash($node)]))
      return;

    op\metaContext(IIData::class)['areConstraintsInferred:' . spl_object_hash($node)] = true;

    $constraints = [];

    foreach (inference\Constraint::processConstraints($node) as $constraint)
      $constraints[] = $constraint;

    foreach ($node->params as $parameter)
      foreach (inference\Constraint::processConstraints($parameter) as $constraint)
        $constraints[] = $constraint;

    if ($node->returnType instanceof Node)
      foreach (inference\Constraint::processConstraints($node->returnType) as $constraint)
        $constraints[] = $constraint;

    $node->stmts = array_merge($constraints, $node->stmts === null ? [] : $node->stmts);

  }

  static function processConstraints ($node) {

    $constraints = [];

    foreach (inference\Attribute::get($node) as $attribute) {

      if ($attribute instanceof Node\Expr\New_ &&
          count($attribute->args) >= 2 &&
          inference\Value::get($attribute->args[0]) == [['type' => 't_string', 'value' => 'constraint']]) {

        $constraint = $attribute->args[1]->value->items[0]->value;

        $constraintExpression = new Node\Expr\FuncCall(new Node\Name('assert'), [$constraint]);

        $constraintExpression->setAttribute('isGenerated', true);
        $constraintExpression->setAttribute('isConstraint', true);

        $constraintExpression->setAttribute('constructTypeName', 'constraint');

        $constraintExpression->setAttribute('displayPrint', $attribute->getAttribute('displayPrint', ''));
        $constraintExpression->setAttribute('startLine', $attribute->getAttribute('startLine', -1));
        $constraintExpression->setAttribute('endLine', $attribute->getAttribute('endLine', -1));

        $constraints[] = $constraintExpression;

      }

      if ($attribute instanceof Node\Expr\New_ &&
          count($attribute->args) >= 2 &&
          inference\Value::get($attribute->args[0]) == [['type' => 't_string', 'value' => 'param']]) {

        if (count($attribute->args[1]->value->items) < 2)
          continue;

        $expressionConstraint = phpLanguage\PhpDocumentor::expressionConstraint(
          $attribute->args[1]->value->items[0]->value->value,
          $attribute->args[1]->value->items[1]->value->value
        );

        if (!$expressionConstraint)
          continue;

        try {
          $constraint = op\metaContext('code')->parse('<?php assert(' . $expressionConstraint . ');');
        } catch (\PhpParser\Error $exception) {

          // @todo: Handle and remove.
          continue;

        }

        assert(is_array($constraint) && count($constraint) == 1);
        $constraint = $constraint[0];

        $constraint->setAttribute('isGenerated', true);
        $constraint->setAttribute('isConstraint', true);

        $constraint->setAttribute('displayPrint', $attribute->getAttribute('displayPrint'));

        $constraint->setAttribute('constructTypeName', 'constraint');

        if ($attribute->getAttribute('path', ''))
          $constraint->setAttribute('path', $attribute->getAttribute('path', ''));

        $constraint->setAttribute('startLine', $attribute->getAttribute('startLine', -1));
        $constraint->setAttribute('endLine', $attribute->getAttribute('endLine', -1));

        $constraints[] = $constraint;

      }

    }

    return $constraints;

  }

}
