<?php

namespace phlint\inference;

use \luka8088\phops as op;
use \phlint\IIData;
use \phlint\inference;
use \PhpParser\Node;

class Concept {

  function getIdentifier () {
    return 'concept';
  }

  function getPass () {
    return 30;
  }

  function getDependencies () {
    return [
      'attribute',
    ];
  }


  /**
   * Get node analysis-time known concepts.
   *
   * @param object $node Node whose concepts to get.
   * @return string[]
   */
  static function get ($node) {

    if ($node === null)
      return [];

    assert(is_object($node));

    if ($node instanceof Node\Arg)
      return self::get($node->value);

    if ($node instanceof Node\Expr\Ternary)
      return array_unique(array_merge(self::get($node->if), self::get($node->else)));

    if ($node instanceof Node\Param)
      if (is_string($node->type) && in_array($node->type, ['array', 'object']))
        return ['o_' . $node->type];

    return [];

  }

}
