<?php

namespace phlint\inference;

use \ArrayObject;
use \luka8088\ExtensionCall;
use \luka8088\ExtensionCallExtends;
use \luka8088\ExtensionInterface;
use \luka8088\phops as op;
use \phlint\IIData;
use \phlint\inference;
use \phlint\inference\Attribute;
use \phlint\inference\Scope;
use \phlint\inference\Symbol;
use \phlint\NodeConcept;
use \phlint\NodeTraverser;
use \phlint\phpLanguage;
use \PhpParser\Comment;
use \PhpParser\Node;

class Type {

  function getIdentifier () {
    return 'type';
  }

  function getPass () {
    return 30;
  }

  function getDependencies () {
    return [
      'attribute',
      'scope',
      'symbol',
      'typeLiteral',
    ];
  }

  /** @see /documentation/glossary/intermediatelyInferredData.md */
  protected $iiData = null;

  function setIIData ($iiData) {
    $this->iiData = $iiData;
  }

  protected $extensionInterface = null;

  function setExtensionInterface ($extensionInterface) {
    $this->extensionInterface = $extensionInterface;
  }

  function beforeTraverse () {

    op\metaContext('code')->data['symbols']['t_mixed'] = [
      'id' => 't_mixed',
      'phpId' => 'mixed',
      'aliasOf' => '',
      'definitionNodes' => [],
    ];

    // @todo: Remove
    foreach (phpLanguage\Fixture::$implicitTypeConversions as $type1 => $types2) {
      if (!isset(op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type1, 'type')]))
        op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type1, 'type')] = [
          'id' => Symbol::fullyQualifiedIdentifier($type1, 'type'),
          'phpId' => $type1,
          'aliasOf' => '',
          'definitionNodes' => [],
        ];
      foreach ($types2 as $type2)
        if (!isset(op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type2, 'type')]))
          op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type2, 'type')] = [
            'id' => Symbol::fullyQualifiedIdentifier($type2, 'type'),
            'phpId' => $type2,
            'aliasOf' => '',
            'definitionNodes' => [],
          ];
    }

    op\metaContext('code')->data['symbols']['t_mixed'] = [
      'id' => 't_mixed',
      'phpId' => 'mixed',
      'aliasOf' => '',
      'declarationNodes' => [],
    ];

    foreach (phpLanguage\Fixture::$implicitTypeConversions as $type1 => $types2) {
      if (!isset(op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type1, 'type')]))
        op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type1, 'type')] = [
          'id' => Symbol::fullyQualifiedIdentifier($type1, 'type'),
          'phpId' => $type1,
          'aliasOf' => '',
          'declarationNodes' => [],
        ];
      foreach ($types2 as $type2)
        if (!isset(op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type2, 'type')]))
          op\metaContext('code')->data['symbols'][Symbol::fullyQualifiedIdentifier($type2, 'type')] = [
            'id' => Symbol::fullyQualifiedIdentifier($type2, 'type'),
            'phpId' => $type2,
            'aliasOf' => '',
            'declarationNodes' => [],
          ];
    }

    foreach (phpLanguage\Fixture::$implicitTypeConversions as $type => $implicitlyConvertableTypes) {
      $symbol = inference\Symbol::identifier($type, 'type');
      assert(is_string($symbol) && $symbol != '');
      if (!isset(op\metaContext('code')->data['symbols'][$symbol]))
        op\metaContext('code')->data['symbols'][$symbol] = [
          'id' => $symbol,
          'phpId' => $type,
          'aliasOf' => '',
          'declarationNodes' => [],
          'linkedNodes' => [],
          'implicitlyConvertable' => [],
        ];
      foreach ($implicitlyConvertableTypes as $implicitlyConvertableType)
        op\metaContext('code')->data['symbols'][$symbol]['implicitlyConvertable'][] =
          inference\Symbol::identifier($implicitlyConvertableType, 'type');
      if (isset(op\metaContext('code')->data['symbols'][$symbol]['implicitlyConvertable']))
        op\metaContext('code')->data['symbols'][$symbol]['implicitlyConvertable'] = array_unique(op\metaContext('code')->data['symbols'][$symbol]['implicitlyConvertable']);
    }

  }

  function visitNode ($node) {

    $this->inferConditionalGuarantees($node);

    $this->inferSymbolTypes($node);

    self::inferTypes($node);

    $this->interTypesDisplay($node);

  }

  function leaveNode ($node) {

    $this->inferExecutionBranchEffect($node);

  }

  function inferConditionalGuarantees ($node) {

    foreach (inference\ConditionalGuarantee::evaluate($node) as $guarantee) {

      foreach ($guarantee['excludesTypes'] as $type) {

        foreach (inference\SymbolLink::get($guarantee['scope']) as $scope)
          foreach (inference\SymbolLink::get($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            if (!isset($this->iiData['types:' . $scopedSymbol])) {
              $d = [];
              $p = $scope;
              while (true) {
                $p = Scope::parent($p);
                $pa = Symbol::concat($p, Symbol::unqualified($symbol));
                if (isset($this->iiData['types:' . $pa])) {
                  $d = $this->iiData['types:' . $pa];
                  break;
                }
                if (!$p)
                  break;
              }

              $this->iiData['types:' . $scopedSymbol] = $d;

            }

            $this->iiData['types:' . $scopedSymbol] = array_filter($this->iiData['types:' . $scopedSymbol], function ($t) use ($type) { return $t != $type; });

          }

      }

      if (count($guarantee['includesTypes']) > 0) {

        $types = [];

        foreach ($guarantee['includesTypes'] as $includesTypesNode)
          foreach (inference\SymbolLink::get($includesTypesNode) as $type)
            $types[] = $type;

        foreach ([$guarantee['scope']->getAttribute('scopeDeclare', '')] as $scope)
          foreach (inference\SymbolLink::get($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            $this->iiData['types:' . $scopedSymbol] = $types;
          }

      }

      foreach ($guarantee['includesConcepts'] as $concept) {

        foreach (inference\SymbolLink::get($guarantee['scope']) as $scope)
          foreach (inference\SymbolLink::get($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            if (!isset($this->iiData['types:' . $scopedSymbol])) {
              $d = [];
              $p = $scope;
              while (true) {
                $p = Scope::parent($p);
                $pa = Symbol::concat($p, Symbol::unqualified($symbol));
                if (isset($this->iiData['types:' . $pa])) {
                  $d = $this->iiData['types:' . $pa];
                  break;
                }
                if (!$p)
                  break;
              }

              $this->iiData['types:' . $scopedSymbol] = $d;

            }

            $this->iiData['types:' . $scopedSymbol] = array_filter($this->iiData['types:' . $scopedSymbol], function ($t) use ($concept) { if ($concept == 'array') return substr($t, -2) == '[]'; return true; });

          }

      }

      foreach ($guarantee['excludesConcepts'] as $concept) {

        foreach (inference\SymbolLink::get($guarantee['scope']) as $scope)
          foreach (inference\SymbolLink::get($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            if (!isset($this->iiData['types:' . $scopedSymbol])) {
              $d = [];
              $p = $scope;
              while (true) {
                $p = Scope::parent($p);
                $pa = Symbol::concat($p, Symbol::unqualified($symbol));
                if (isset($this->iiData['types:' . $pa])) {
                  $d = $this->iiData['types:' . $pa];
                  break;
                }
                if (!$p)
                  break;
              }

              $this->iiData['types:' . $scopedSymbol] = $d;

            }

            $this->iiData['types:' . $scopedSymbol] = array_filter($this->iiData['types:' . $scopedSymbol], function ($t) use ($concept) { if ($concept == 'array') return substr($t, -2) == '[]'; return true; });

          }

      }

    }

  }

  function inferExecutionBranchEffect ($node) {

    if ($node instanceof Node\Stmt\If_) {

      $guarantees = inference\ConditionalGuarantee::evaluateCondition($node->cond);

      foreach ($guarantees as $guarantee) {

        $scope = $node->getAttribute('scope', '');

        $types = [];

        foreach ($guarantee['includesTypes'] as $includesTypesNode)
          foreach (inference\SymbolLink::get($includesTypesNode) as $type)
            $types[] = $type;

        foreach (inference\SymbolLink::get($guarantee['node']) as $symbol) {

          $symbolx = Symbol::concat($scope, Symbol::unqualified($symbol));

          if (isset($this->iiData['types:' . $symbol]) && count($this->iiData['types:' . $symbol]) > 0)
            if (isset($this->iiData['types:' . $symbolx]))
              $this->iiData['types:' . $symbolx] = array_diff(
                $this->iiData['types:' . $symbolx],
                $types
              );

        }

      }

    }

  }

  function inferSymbolTypes ($node) {

    if ($node instanceof Node\Expr\Assign)
      $this->registerSymbolsTypes($node->var, inference\Type::get($node->expr));

    if ($node instanceof Node\Expr\Assign) {

      if ($node->var instanceof Node\Expr\ArrayDimFetch) {

        if (!$node->var->dim || count($node->var->var->getAttribute('isInitialization', [])) > 0)
        $this->registerSymbolsTypes($node->var->var, array_filter(array_map(function ($type) { if ($type == 't_undefined') return ''; return $type . '[]'; }, inference\Type::get($node->expr))));

      }

      if ($node->var instanceof Node\Expr\Variable) {
        foreach (inference\Attribute::get($node->var) as $attribute) {

          if ($attribute instanceof Node\Expr\New_ &&
              count($attribute->args) >= 1 &&
              inference\Value::get($attribute->args[0]) == [['type' => 't_string', 'value' => 'var']]) {

            foreach ([$node->var->getAttribute('scope', '')] as $scope) {
              $typeSymbol = SymbolLink::lookupSinglePhpId($attribute->args[1]->value->items[0]->value->value, $scope);
              if ($typeSymbol)
                $this->registerSymbolsTypes($node->var, [$typeSymbol]);
            }
          }
        }
      }

    }

    if ($node instanceof Node\Expr\Closure)
      foreach ($node->uses as $use_)
        $this->registerSymbolsTypes($use_, inference\Type::lookupSymbol(
          Symbol::concat($node->getAttribute('scope', ''), Symbol::identifier($use_->var, 'variable'))
        ));

    if ($node instanceof Node\Expr\ClosureUse)
      $this->registerSymbolsTypes($node, inference\Type::get($node));

    if ($node instanceof Node\Param && count(inference\NodeRelation::contextNode($node)->stmts) > 0)
      $this->registerSymbolsTypes($node, inference\Type::get($node));

    if ($node instanceof Node\Param && count(inference\NodeRelation::contextNode($node)->stmts) > 0)
      foreach (inference\Attribute::get($node) as $attribute) {
        if ($attribute instanceof Node\Expr\New_ &&
            count($attribute->args) >= 1 &&
            inference\Value::get($attribute->args[0]) == [['type' => 't_string', 'value' => 'var']])
        if (isset($attribute->args[1]->value->items[0])) {
          foreach ([$node->getAttribute('scope', '')] as $scope) {
            $typeSymbol = SymbolLink::lookupSinglePhpId($attribute->args[1]->value->items[0]->value->value, $scope);
            if ($typeSymbol)
              $this->registerSymbolsTypes($node, [$typeSymbol]);
          }
        }
      }

    if ($node instanceof Node\Stmt\Foreach_) {
      if ($node->keyVar)
        $this->registerSymbolsTypes($node->keyVar, inference\Type::get($node->keyVar));
      if ($node->expr && $node->valueVar) {
        $valueTypes = [];
        foreach (inference\Type::get($node->expr) as $type)
          if (substr($type, -2) == '[]')
            $valueTypes[] = substr($type, 0, -2);
        $this->registerSymbolsTypes($node->valueVar, array_unique(array_merge(inference\Type::get($node->valueVar), $valueTypes)));
      }
    }

    if (inference\Execution::isReachable($node))
    if ($node instanceof Node\Stmt\Return_) {
      foreach ([$node->getAttribute('scope', '')] as $scope) {
        $scope = Scope::contextScope($scope);
        if ($scope) {
          $this->registerSymbolsTypes([$scope . '.r'], array_unique(array_merge(
            isset($this->iiData['types:' . $scope . '.r']) ? $this->iiData['types:' . $scope . '.r'] : [],
            inference\Type::get($node->expr)
          )));
        }
      }
    }

  }

  static function inferTypes ($node) {

    if ($node instanceof Node\Expr\Assign || $node instanceof Node\Expr\AssignRef)
      op\metaContext(IIData::class)['nodeTypes:' . spl_object_hash($node->var)] = inference\Type::get($node->expr);

    if ($node instanceof Node\Expr\AssignOp)
      op\metaContext(IIData::class)['nodeTypes:' . spl_object_hash($node->var)] = inference\Type::get($node->var);

  }

  function registerSymbolsTypes ($symbols, $types) {

    if ($symbols instanceof Node)
      $symbols = inference\SymbolLink::get($symbols);

    foreach ($symbols as $symbol) {

      if (!$symbol)
        continue;

      $this->iiData['types:' . $symbol] = [];

      foreach (Symbol::visibleScopes($symbol) as $scopeSymbol) {
        if (!isset($this->iiData['types:' . $scopeSymbol]))
          $this->iiData['types:' . $scopeSymbol] = [];
        $this->iiData['types:' . $scopeSymbol] = array_unique(array_merge($this->iiData['types:' . $scopeSymbol], $types));
      }

    }

  }

  /**
   * Lookup the node types.
   * Note that this call can be significantly expensive.
   * For general purpose it is better to call `::get` which will
   * call lookup implicitly if needed.
   *
   * @internal
   */
  static function lookup ($node) {

    if ($node instanceof Node\Expr\Array_) {
      $itemKeyTypes = [];
      $itemTypes = [];
      foreach ($node->items as $itemNode) {
        $itemKeyNodeTypes = inference\Type::get($itemNode->key);
        if (count($itemKeyNodeTypes) == 0)
          $itemKeyTypes[] = '';
        foreach ($itemKeyNodeTypes as $itemKeyNodeType)
          $itemKeyTypes[] = $itemKeyNodeType;
        $itemNodeTypes = inference\Type::get($itemNode);
        if (count($itemNodeTypes) == 0)
          $itemTypes[] = '';
        foreach ($itemNodeTypes as $itemNodeType)
          $itemTypes[] = $itemNodeType;
      }
      $commonKey = inference\Type::composeMulti(array_unique($itemKeyTypes));
      $common = inference\Type::composeMulti(array_unique($itemTypes));
      if ($common)
        return [inference\Type::composeArray($commonKey, $common)];
      return [];
    }

    if ($node instanceof Node\Expr\BinaryOp\Concat) {
      $types = [];
      foreach (Type::get($node->left) as $leftType)
        foreach (Type::get($node->right) as $rightType) {
          if (in_array($leftType, ['t_int', 't_autoInteger', 't_stringInt', 't_autoBool', 't_stringBool']) && in_array($rightType, ['t_int', 't_autoInteger', 't_stringInt', 't_autoBool', 't_stringBool'])) {
            $types[] = 't_stringInt';
            continue;
          }
          if (in_array($leftType, ['t_int', 't_autoInteger', 't_stringInt', 't_autoBool', 't_stringBool']) && in_array($rightType, ['t_float', 't_autoFloat', 't_stringFloat'])) {
            $types[] = 't_stringFloat';
            continue;
          }
          $types[] = 't_string';
        }
      return array_unique($types);
    }

    if ($node instanceof Node\Expr\Closure)
      return inference\SymbolLink::get($node);

    if ($node instanceof Node\Expr\New_)
      return inference\SymbolLink::get($node);

    if ($node instanceof Node\Name)
      return inference\SymbolLink::get($node);

    if (NodeConcept::isInvocationNode($node)) {
      $types = [];
      foreach (inference\SymbolLink::get($node) as $symbol)
        if (isset(op\metaContext('code')->data['symbols'][$symbol]['declarationNodes']))
          foreach (op\metaContext('code')->data['symbols'][$symbol]['declarationNodes'] as $declarationNode)
              if (NodeConcept::isExecutionContextNode($declarationNode))
                foreach (inference\Type::getReturn($declarationNode) as $type)
                  $types[] = $type;
      return array_unique($types);
    }

    // @todo: Remove
    if ($node instanceof Node\Param && $node->type)
      return array_filter(inference\SymbolLink::get($node->type), function ($z) {
        return $z == 't_float' || strpos($z, '[]') !== false;
      });

    if ($node instanceof Node\Param && $node->type)
      return inference\SymbolLink::get($node->type);

    if ($node instanceof Node\Param) {
      $types = [];
      foreach (inference\Attribute::get($node) as $attribute)
      if ($attribute instanceof Node\Expr\New_ &&
          count($attribute->args) >= 1 &&
          inference\Value::get($attribute->args[0]) == [['type' => 't_string', 'value' => 'param']])
      if (isset($attribute->args[1], $attribute->args[1]->value->items[0])) {
        $parameterTypeRaw = inference\Value::get($attribute->args[1]->value->items[0]->value->value);
        assert(count($parameterTypeRaw) == 1);
        if (strpos($parameterTypeRaw[0]['value'], '$') !== 0) {
          // @todo: Rewrite.
          if (count(array_intersect(explode('|', $parameterTypeRaw[0]['value']), ['', 'array', 'mixed', 'object'])) == 0)
            foreach (array_filter(array_map(function ($phpDocType) use ($node) { return SymbolLink::lookupSinglePhpId($phpDocType, $node->getAttribute('scope', '')); }, explode('|', $parameterTypeRaw[0]['value']))) as $type)
              $types[] = $type;
        }
      }
      return $types;
    }

    if ($node instanceof Node) {
      $types = [];
      foreach (inference\SymbolLink::get($node) as $symbol)
        foreach (inference\Type::lookupSymbol($symbol) as $type)
          $types[] = $type;
      return array_unique($types);
    }

    return [];

  }

  /**
   * Lookup the types that a symbol would yield if it
   * would be, for example, evaluated as an expression.
   *
   * @param string $symbol
   * @return string[]
   */
  static function lookupSymbol ($symbol) {

    $types = [];

    $unqualifiedSymbol = Symbol::unqualified($symbol);
    foreach (Scope::visibleScopes(Scope::symbolScope($symbol)) as $visibleScope) {
      $lookupSymbol = Symbol::concat($visibleScope, $unqualifiedSymbol);
      if (isset(op\metaContext(IIData::class)['types:' . $lookupSymbol])) {
        foreach (op\metaContext(IIData::class)['types:' . $lookupSymbol] as $type)
          $types[] = $type;
        break;
      }
    }

    return array_unique($types);

  }

  function interTypesDisplay ($node) {

    if (!($node instanceof Node\Expr\ClosureUse) && !($node instanceof Node\Param))
      return;

    $typesDisplay = [];

    foreach (inference\Type::get($node) as $type) {
      $typesDisplay[] = inference\Symbol::phpID($type);
    }

    if (count($typesDisplay) > 0)
      $node->setAttribute('typesDisplay', array_unique($typesDisplay));

  }

  /**
   * Get node analysis-time known types.
   *
   * @param object $node Node whose type to get.
   * @return string[]
   */
  static function get ($node) {

    if ($node === null)
      return [];

    assert(is_object($node));

    if ($node instanceof Node\Arg)
      return self::get($node->value);

    if ($node instanceof Node\Expr\ArrayDimFetch)
      return array_map(function ($arrayType) {
        return substr($arrayType, -2) == '[]' ? substr($arrayType, 0, -2) : 't_mixed';
      }, self::get($node->var));

    if ($node instanceof Node\Expr\ArrayItem)
      return self::get($node->value);

    if ($node instanceof Node\Expr\BinaryOp\BooleanAnd)
      return ['t_bool'];

    if ($node instanceof Node\Expr\Ternary)
      return array_unique(array_merge(self::get($node->if), self::get($node->else)));

    if ($node instanceof Node\Param && is_string($node->type) && in_array($node->type, ['string', 'int', 'bool']))
      return ['t_' . strtolower($node->type)];

    if ($node instanceof Node\Param && $node->type instanceof Node\Name)
      return self::get($node->type);

    if ($node instanceof Node\Scalar\DNumber)
      return ['t_float'];

    if ($node instanceof Node\Scalar\LNumber)
      return ['t_int'];

    if ($node instanceof Node\Scalar\MagicConst) {
      if ($node instanceof Node\Scalar\MagicConst\Line)
        return ['t_int'];
      return ['t_string'];
    }

    if ($node instanceof Node\Scalar\String_) {
      if ($node->value === '0' || $node->value === '1')
        return ['t_stringBool'];
      if (((string) ((int) $node->value)) === (string) $node->value)
        return ['t_stringInt'];
      if (is_numeric($node->value))
        return ['t_stringFloat'];
      return ['t_string'];
    }

    if (!isset(op\metaContext(IIData::class)['nodeTypes:' . spl_object_hash($node)]))
      op\metaContext(IIData::class)['nodeTypes:' . spl_object_hash($node)] = Type::lookup($node);

    return op\metaContext(IIData::class)['nodeTypes:' . spl_object_hash($node)];

  }

  /**
   * Get node analysis-time known return types.
   *
   * @param object $node Node whose return types to get.
   * @return string[]
   */
  static function getReturn ($node) {

    if (!isset(op\metaContext(IIData::class)['returnTypes:' . spl_object_hash($node)])) {

      $returnTypes = [];

      if ($node->returnType)
        foreach (inference\SymbolLink::get($node->returnType) as $returnType)
          $returnTypes[] = $returnType;

      // @todo: Refactor.
      foreach (inference\Attribute::get($node) as $attribute) {
        if ($attribute instanceof Node\Expr\New_ &&
            count($attribute->args) >= 1 &&
            inference\Value::get($attribute->args[0]) == [['type' => 't_string', 'value' => 'return']])
        if (isset($attribute->args[1]->value->items[0]))
          foreach (inference\Value::get($attribute->args[1]->value->items[0]->value->value) as $value)
            $returnTypes[] = SymbolLink::lookupSinglePhpId(
              $value['value'],
              $node->getAttribute('scope', ''),
              $node->getAttribute('inAnalysisScope', true)
            );
      }

      foreach (inference\Type::lookupSymbol(inference\Symbol::get($node) . '.r') as $returnType)
        $returnTypes[] = $returnType;

      op\metaContext(IIData::class)['returnTypes:' . spl_object_hash($node)] = array_unique($returnTypes);

    }

    return op\metaContext(IIData::class)['returnTypes:' . spl_object_hash($node)];

  }

  /**
   * Is type `$type1` implicitly convertible to type `$type2`.
   *
   * Strict type comparison is done according to rules of `declare(strict_types=1);`.
   * @see http://php.net/manual/en/functions.arguments.php#functions.arguments.type-declaration.strict
   *
   * @param string $type1
   * @param string $type2
   * @param bool $strictType Do a strict type comparison.
   * @return bool
   */
  static function isImplicitlyConvertible ($type1, $type2, $strictType = false, $redundants = []) {

    // @todo: Remove
    if ($type1 == 't_array')
      $type1 = '[]';

    // @todo: Remove
    if ($type2 == 't_array')
      $type2 = '[]';

    if ($type1 == $type2)
      return true;

    if (inference\Type::isArray($type1) && $type2 == 'o_array')
      return true;

    // @todo: Remove
    if (!$strictType)
    if (in_array($type1, ['t_int', 't_autoInteger', 't_autoFloat', 't_stringInt']) && $type2 == 't_float')
      return true;

    if (inference\Type::isMulti($type1)) {
      foreach (inference\Type::decomposeMulti($type1) as $subType1)
        if (!self::isImplicitlyConvertible($subType1, $type2, $strictType, $redundants))
          return false;
      return true;
    }

    if (inference\Type::isMulti($type2)) {
      foreach (inference\Type::decomposeMulti($type2) as $subType2)
        if (self::isImplicitlyConvertible($type1, $subType2, $strictType, $redundants))
          return true;
      return false;
    }

    if (inference\Type::isArray($type1) && inference\Type::isArray($type2)) {
      $decomposedArray1 = inference\Type::decomposeArray($type1);
      $decomposedArray2 = inference\Type::decomposeArray($type2);
      return self::isImplicitlyConvertible($decomposedArray1['keyType'], $decomposedArray2['keyType'], $strictType)
        && self::isImplicitlyConvertible($decomposedArray1['valueType'], $decomposedArray2['valueType'], $strictType)
      ;
    }

    if (!isset(op\metaContext('code')->data['symbols'][$type1]))
      return false;

    if (isset(op\metaContext('code')->data['symbols'][$type1]['aliasOf'])
        && op\metaContext('code')->data['symbols'][$type1]['aliasOf'])
      return self::isImplicitlyConvertible(
        op\metaContext('code')->data['symbols'][$type1]['aliasOf'],
        $type2,
        array_merge($redundants, [$type1])
      );

    if (!isset(op\metaContext('code')->data['symbols'][$type1]['implicitlyConvertable']))
      return false;

    foreach (array_diff(op\metaContext('code')->data['symbols'][$type1]['implicitlyConvertable'], $redundants)
        as $implicitlyConvertableType)
      if (self::isImplicitlyConvertible(
            $implicitlyConvertableType,
            $type2,
            array_merge($redundants, op\metaContext('code')->data['symbols'][$type1]['implicitlyConvertable'])
          ))
        return true;

    return false;

  }

  /**
   * Is type traversable?
   *
   * @param string $type Type to check.
   * @return bool
   */
  static function isTraversable ($type) {

    // @todo: Remove.
    if (in_array($type, ['t_mixed', 't_array']))
      return true;

    if (substr($type, -2) == '[]')
      return true;

    if (inference\Type::isImplicitlyConvertible($type, 'c_traversable'))
      return true;

    return false;

  }

  /**
   * Is type `$type` an array?
   *
   * @param string $type
   * @return bool
   */
  static function isArray ($type) {
    if (substr($type, -1) != ']')
      return false;
    $tokens = token_get_all('<?php ' . $type);
    $curlyBracketsNestingCount = 0;
    $roundBracketsNestingCount = 0;
    $squareBracketsNestingCount = 0;
    for ($i = count($tokens) - 1; $i > 0; $i -= 1) {
      if ($tokens[$i] == '}')
        $curlyBracketsNestingCount += 1;
      if ($tokens[$i] == '{')
        $curlyBracketsNestingCount -= 1;
      if ($tokens[$i] == ')')
        $roundBracketsNestingCount += 1;
      if ($tokens[$i] == '(')
        $roundBracketsNestingCount -= 1;
      if ($tokens[$i] == ']')
        $squareBracketsNestingCount += 1;
      if ($tokens[$i] == '[')
        $squareBracketsNestingCount -= 1;
      if ($curlyBracketsNestingCount == 0 && $roundBracketsNestingCount == 0
          && $squareBracketsNestingCount == 0 && $tokens[$i] == '|')
        return false;
    }
    return true;
  }

  /**
   * Compose a key type and value type into an array type.
   *
   * @param string $keyType
   * @param string $valueType
   * @return string
   */
  static function composeArray ($keyType, $valueType) {
    return (strpos($valueType, '|') !== false
        ? '(' . $valueType . ')'
        : $valueType)
      . '[' . ($keyType == 't_int' ? '' : $keyType) . ']';
  }

  /**
   * Decompose an array type to a key type and value type.
   *
   * @param string $type
   * @return ['keyType' => string, 'valueType' => string]
   */
  static function decomposeArray ($type) {
    assert(count(debug_backtrace()) < 512);
    $tokens = token_get_all('<?php ' . $type);
    $decomposed = [
      'keyType' => '',
      'valueType' => '',
    ];
    if ($tokens[count($tokens) - 1] != ']')
      return $decomposed;
    $curlyBracketsNestingCount = 0;
    $roundBracketsNestingCount = 0;
    $squareBracketsNestingCount = 0;
    $keyLength = 0;
    for ($i = count($tokens) - 1; $i > 0; $i -= 1) {
      $keyLength += strlen(is_array($tokens[$i]) ? $tokens[$i][1] : $tokens[$i]);
      if ($tokens[$i] == '}')
        $curlyBracketsNestingCount += 1;
      if ($tokens[$i] == '{')
        $curlyBracketsNestingCount -= 1;
      if ($tokens[$i] == ')')
        $roundBracketsNestingCount += 1;
      if ($tokens[$i] == '(')
        $roundBracketsNestingCount -= 1;
      if ($tokens[$i] == ']')
        $squareBracketsNestingCount += 1;
      if ($tokens[$i] == '[')
        $squareBracketsNestingCount -= 1;
      if ($curlyBracketsNestingCount == 0 && $roundBracketsNestingCount == 0 && $squareBracketsNestingCount == 0) {
        $decomposed['keyType'] = substr($type, -$keyLength + 1, -1);
        if (substr($decomposed['keyType'], 0, 1) == '(' && substr($decomposed['keyType'], -1) == ')')
          $decomposed['keyType'] = substr($decomposed['keyType'], 1, -1);
        $decomposed['valueType'] = substr($type, 0, -$keyLength);
        if (substr($decomposed['valueType'], 0, 1) == '(' && substr($decomposed['valueType'], -1) == ')')
          $decomposed['valueType'] = substr($decomposed['valueType'], 1, -1);
        break;
      }
    }
    return $decomposed;
  }

  /**
   * Is type `$type` a multi-type?
   *
   * @param string $type
   * @return bool
   */
  static function isMulti ($type) {

    // @todo: Remove
    while (substr($type, 0, 1) == '(' && substr($type, -1) == ')')
      $type = substr($type, 1, -1);

    if (strpos($type, '|') === false)
      return false;
    $tokens = token_get_all('<?php ' . $type);
    $curlyBracketsNestingCount = 0;
    $roundBracketsNestingCount = 0;
    $squareBracketsNestingCount = 0;
    for ($i = count($tokens) - 1; $i > 0; $i -= 1) {
      if ($tokens[$i] == '}')
        $curlyBracketsNestingCount += 1;
      if ($tokens[$i] == '{')
        $curlyBracketsNestingCount -= 1;
      if ($tokens[$i] == ')')
        $roundBracketsNestingCount += 1;
      if ($tokens[$i] == '(')
        $roundBracketsNestingCount -= 1;
      if ($tokens[$i] == ']')
        $squareBracketsNestingCount += 1;
      if ($tokens[$i] == '[')
        $squareBracketsNestingCount -= 1;
      if ($curlyBracketsNestingCount == 0 && $roundBracketsNestingCount == 0
          && $squareBracketsNestingCount == 0 && $tokens[$i] == '|')
        return true;
    }
    return false;
  }

  /**
   * Compose a list of types into a multi-type.
   *
   * @param string[] $types
   * @return string
   */
  static function composeMulti ($types) {
    $subTypes = [];
    foreach ($types as $aType)
      foreach (inference\Type::isMulti($aType) ? inference\Type::decomposeMulti($aType) : [$aType] as $subType)
        $subTypes[] = $subType;
    return implode('|', array_unique($subTypes));
  }

  /**
   * Decompose a multi-type to a list of types.
   *
   * @param string $type
   * @return string[]
   */
  static function decomposeMulti ($type) {

    // @todo: Remove
    while (substr($type, 0, 1) == '(' && substr($type, -1) == ')')
      $type = substr($type, 1, -1);

    $tokens = token_get_all('<?php ' . $type);
    $decomposed = [];
    $curlyBracketsNestingCount = 0;
    $roundBracketsNestingCount = 0;
    $squareBracketsNestingCount = 0;
    $lastTypePosition = 0;
    $thisTokenEndPosition = 0;
    for ($i = 1; $i < count($tokens); $i += 1) {
      $thisTokenEndPosition += strlen(is_array($tokens[$i]) ? $tokens[$i][1] : $tokens[$i]);
      if ($tokens[$i] == '{')
        $curlyBracketsNestingCount += 1;
      if ($tokens[$i] == '}')
        $curlyBracketsNestingCount -= 1;
      if ($tokens[$i] == '(')
        $roundBracketsNestingCount += 1;
      if ($tokens[$i] == ')')
        $roundBracketsNestingCount -= 1;
      if ($tokens[$i] == '[')
        $squareBracketsNestingCount += 1;
      if ($tokens[$i] == ']')
        $squareBracketsNestingCount -= 1;
      if ($curlyBracketsNestingCount == 0 && $roundBracketsNestingCount == 0
          && $squareBracketsNestingCount == 0 && $tokens[$i] == '|') {
        $decomposed[] = substr($type, $lastTypePosition, $thisTokenEndPosition - $lastTypePosition - 1);
        $lastTypePosition = $thisTokenEndPosition;
      }
    }
    $decomposed[] = substr($type, $lastTypePosition, $thisTokenEndPosition - $lastTypePosition);
    return $decomposed;
  }

}
